/**
 * Created by Developer9 on 3/26/2015.
 */
var pg = require("pg");
//var http = require("http")
util = require('../util.js');
var logger = util.get_logger("db_connect");
var api = require('../route/master/api.js')
exports.get_pg_connection = function() {
    var conString = util.get_connection_string_pg();//Get Dynamic Connection String with Appropriate DB,User,Pass etc. froom utill.js

    var client = new pg.Client(conString);
    client.connect();
}
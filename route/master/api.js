/**
 * Created by Developer9 on 3/26/2015.
 */
var pg = require("pg");
var util = require('../../util.js');
var token = require('../../token.js')

shortid = require('shortid')
var aws = require('aws-sdk');
var knox = require('knox');
var Busboy = require('busboy');

var fs = require('fs');
var path = require('path');

var async = require('async');
var express = require('express');
var router = express.Router();

var logger = util.get_logger("api");

exports.renew_token = function(req, res, next){
    var token_data = token.verify(req.headers.token);
    var token_renw="";
    if(token_data) {
        token_renw = token.assign(token_data);
    }
    util.send_to_response(token_renw, res);

}

//add driver
exports.add_driver = function(req, res) {
    var x=req.body.data;
    var conString = util.get_connection_string_pg();
    pg.connect(conString, function(err, client, done) {
        if(err) res.send("Could not connect to DB: " + err);
        client.query('INSERT INTO driver (driver_id, driver_first_name,driver_last_name,driver_dob,driver_verification_status,driver_yrs_exp,driver_profile_pic,driver_eye_status,driver_health_status,driver_status,driver_org_id,driver_created_by,driver_created_dt) VALUES ($1, $2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13)',
            [x.driver_id, x.driver_first_name,x.driver_last_name,x.driver_dob,x.driver_verification_status,x.driver_yrs_exp,x.driver_profile_pic,x.driver_eye_status,x.driver_health_status,x.driver_status,x.driver_org_id,x.driver_created_by,new Date],
            function(err, result) {
                done();
                if(err) return res.send(err);
                res.send('Record Inserted Successfully');
            });
    });

}

//update driver
exports.update_driver = function(req, res) {
    var x=req.body.data;
    var conString = util.get_connection_string_pg();
    pg.connect(conString, function(err, client, done) {
        if(err) res.send("Could not connect to DB: " + err);
        client.query("UPDATE driver SET driver_first_name=($1),driver_last_name=($2),driver_dob=($3),driver_verification_status=($4),driver_yrs_exp=($5),driver_profile_pic=($6),driver_eye_status=($7),driver_health_status=($8),driver_status=($9),driver_org_id=($10),driver_modified_by=($11),driver_modified_dt=($12) WHERE driver_id=($13)",[x.driver_first_name,x.driver_last_name,x.driver_dob,x.driver_verification_status,x.driver_yrs_exp,x.driver_profile_pic,x.driver_eye_status,x.driver_health_status,x.driver_status,x.driver_org_id,x.driver_modified_by,new Date,x.driver_id],
            function(err, result) {
                done();
                if(err) return res.send(err);
                res.send('Record Updated Successfully');
            });
    });

}

//Get Driver details
exports.get_driver_details = function(req, res) {
    var conString = util.get_connection_string_pg();
    var retData = {};
    var client = new pg.Client(conString);


// Now you can start querying your database. Here is a sample.

    client.connect(function(err) {
        if (err) {
            return console.error('could not connect to postgresq',err);
        }
        var query="";
        if(req.body.driver_id=="" || req.body.driver_id==null || req.body.driver_id===undefined){
            query = "SELECT * FROM \"driver\";"
        }
        else{
            query = "SELECT * FROM \"driver\" where driver_id='" + req.body.driver_id + "';"
        }
        client.query(query, function(err, result) {
            if (err) {
                return console.err("could not complete query", err);
            }
            client.end();
            console.log(result.rows);
            if(result.rows.length>0){
                res.send(result.rows);
            }
            else{
                res.send(result.rows);
            }

        });
    })
}


////Add Transporter
//exports.add_transporter = function(req, res) {
//    var conString = util.get_connection_string_pg();
//    var retData = {};
//    var client = new pg.Client(conString);
//    var x=req.body.data;
//
//// Now you can start querying your database. Here is a sample.
//
//    client.connect(function(err) {
//        if (err) {
//            return console.error('could not connect to postgresq',err);
//        }
////        var query = "SELECT * FROM INSERT_INTO_TRANSPORTER  ('"    + x.trans_id + "','"
////            + x.trans_name + "', '"
////            + x.trans_p_contact  + "','"
////            + x.trans_email  + "','"
////            + x.trans_mobile  + "','"
////            + x.trans_land  + "','"
////            + x.trans_city  + "','"
////            + x.trans_state  + "','"
////            + x.trans_country  + "','"
////            + x.trans_created_by  + "','"
////            + new Date()  + "','"
////            + new Date()  + "','"
////            + x.trans_status  + "','"
////            + x.trans_org_id  + "')";
//        var query="select INSERT_INTO_TRANSPORTER (1001,$1,'f','a','b','c','d','e','f',1001,'2014-07-15'::timestamp,'2014-07-15'::timestamp,4,1001);"
//        client.query(query,['a'], function(err, result) {
//            if (err) {
//                return console.err("could not complete query", err);
//            }
//            client.end();
//            console.log(result.rows);
//            if(result.rows.length>0){
//                res.send(result.rows);
//            }
//            else{
//                res.send(result.rows);
//            }
//
//        });
//    })
//
//}

//add transporter
exports.add_transporter = function(req, res) {
    var x=req.body.data;
    var conString = util.get_connection_string_pg();
    pg.connect(conString, function(err, client, done) {
        if(err) res.send("Could not connect to DB: " + err);
        client.query('INSERT INTO transporter (trans_id, trans_name,trans_p_contact,trans_email,trans_mobile,trans_land,trans_city,trans_state,trans_country,trans_created_by,trans_created_dt,trans_registration_dt,trans_status,trans_org_id) VALUES ($1, $2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)',
            [x.trans_id, x.trans_name,x.trans_p_contact,x.trans_email,x.trans_mobile,x.trans_land,x.trans_city,x.trans_state,x.trans_country,x.trans_created_by,new Date,new Date,x.trans_status,x.trans_org_id],
            function(err, result) {
                done();
                if(err) return res.send(err);
                res.send('Record Inserted Successfully');
            });
    });

}


//update transporter
exports.update_transporter = function(req, res) {
    var x=req.body.data;
    var conString = util.get_connection_string_pg();
    pg.connect(conString, function(err, client, done) {
        if(err) res.send("Could not connect to DB: " + err);
        client.query("UPDATE transporter SET trans_name=($1),trans_p_contact=($2),trans_email=($3),trans_mobile=($4),trans_land=($5),trans_city=($6),trans_state=($7),trans_country=($8),trans_modified_by=($9),trans_modified_dt=($10),trans_registration_dt=($11),trans_status=($12),trans_org_id=($13) WHERE trans_id=($14)",[x.trans_name,x.trans_p_contact,x.trans_email,x.trans_mobile,x.trans_land,x.trans_city,x.trans_state,x.trans_country,x.trans_modified_by,new Date,new Date,x.trans_status,x.trans_org_id,x.trans_id],
            function(err, result) {
                done();
                if(err) return res.send(err);
                res.send('Record Updated Successfully');
            });
    });

}

//Get Transporter details
exports.get_transporter_details = function(req, res) {
    var conString = util.get_connection_string_pg();
    var retData = {};
    var client = new pg.Client(conString);


// Now you can start querying your database. Here is a sample.

    client.connect(function(err) {
        if (err) {
            return console.error('could not connect to postgresq',err);
        }
        var query="";
        if(req.body.trans_id=="" || req.body.trans_id==null || req.body.trans_id===undefined){
            query = "SELECT * FROM \"transporter\";"
        }
        else{
            query = "SELECT * FROM \"transporter\" where trans_id='" + req.body.trans_id + "';"
        }
        client.query(query, function(err, result) {
            if (err) {
                return console.err("could not complete query", err);
            }
            client.end();
            console.log(result.rows);
            if(result.rows.length>0){
                res.send(result.rows);
            }
            else{
                res.send(result.rows);
            }

        });
    })
}

//add organization
exports.add_organization = function(req, res) {
    var x=req.body.data;
    var conString = util.get_connection_string_pg();
    pg.connect(conString, function(err, client, done) {
        if(err) res.send("Could not connect to DB: " + err);
        client.query('INSERT INTO organization (org_id,org_name,org_address,org_city,org_state ,org_country ,org_emergency_no1,org_emergency_no2,org_created_by,org_created_dt,org_status) VALUES ($1, $2,$3,$4,$5,$6,$7,$8,$9,$10,$11)',
            [x.org_id, x.org_name,x.org_address,x.org_city,x.org_state,x.org_country,x.org_emergency_no1,x.org_emergency_no2,x.org_created_by,new Date,x.org_status],
            function(err, result) {
                done();
                if(err) return res.send(err);
                res.send('Record Inserted Successfully');
            });
    });

}


//update organization
exports.update_organization = function(req, res) {
    var x=req.body.data;
    var conString = util.get_connection_string_pg();
    pg.connect(conString, function(err, client, done) {
        if(err) res.send("Could not connect to DB: " + err);
        client.query("UPDATE organization SET org_name=($1),org_address=($2),org_city=($3),org_state=($4),org_country=($5),org_emergency_no1=($6),org_emergency_no2=($7),org_modified_by=($8),org_modified_dt=($9),org_status=($10) WHERE org_id=($11)",[x.org_name,x.org_address,x.org_city,x.org_state,x.org_country,x.org_emergency_no1,x.org_emergency_no2,x.org_modified_by,new Date,x.org_status,x.org_id],
            function(err, result) {
                done();
                if(err) return res.send(err);
                res.send('Record Updated Successfully');
            });
    });

}


//Get organization details
exports.get_organization_details = function(req, res) {
    var conString = util.get_connection_string_pg();
    var retData = {};
    var client = new pg.Client(conString);


// Now you can start querying your database. Here is a sample.

    client.connect(function(err) {
        if (err) {
            return console.error('could not connect to postgresq',err);
        }
        var query="";
        if(req.body.org_id=="" || req.body.org_id==null || req.body.org_id===undefined){
            query = "SELECT * FROM \"organization\";"
        }
        else{
            query = "SELECT * FROM \"organization\" where org_id='" + req.body.org_id + "';"
        }
        client.query(query, function(err, result) {
            if (err) {
                return console.err("could not complete query", err);
            }
            client.end();
            console.log(result.rows);
            if(result.rows.length>0){
                res.send(result.rows);
            }
            else{
                res.send(result.rows);
            }

        });
    })
}

//add vehicle
exports.add_vehicle = function(req, res) {
    var x=req.body.data;
    var conString = util.get_connection_string_pg();
    pg.connect(conString, function(err, client, done) {
        if(err) res.send("Could not connect to DB: " + err);
        client.query('INSERT INTO vehicle (vehicle_no,vehicle_registration_dt,vehicle_type,vehicle_transporter,vehicle_last_maintenance,vehicle_status,vehicle_org_id,vehicle_created_by,vehicle_created_dt) VALUES ($1, $2,$3,$4,$5,$6,$7,$8,$9)',
            [x.vehicle_no, new Date,x.vehicle_type,x.vehicle_transporter,x.vehicle_last_maintenance,x.vehicle_status, x.vehicle_org_id,x.vehicle_created_by,new Date],
            function(err, result) {
                done();
                if(err) return res.send(err);
                res.send('Record Inserted Successfully');
            });
    });

}


//update vehicle
exports.update_vehicle = function(req, res) {
    var x=req.body.data;
    var conString = util.get_connection_string_pg();
    pg.connect(conString, function(err, client, done) {
        if(err) res.send("Could not connect to DB: " + err);
        client.query("UPDATE vehicle SET vehicle_registration_dt=($1),vehicle_type=($2),vehicle_transporter=($3),vehicle_last_maintenance=($4),vehicle_status=($5),vehicle_org_id=($6),vehicle_modified_by=($7),vehicle_modified_dt=($8) WHERE vehicle_no=($9)",[x.vehicle_registration_dt,x.vehicle_type,x.vehicle_transporter,x.vehicle_last_maintenance,x.vehicle_status,x.vehicle_org_id,x.vehicle_modified_by,new Date],
            function(err, result) {
                done();
                if(err) return res.send(err);
                res.send('Record Updated Successfully');
            });
    });

}


//Get vehicle details
exports.get_vehicle_details = function(req, res) {
    var conString = util.get_connection_string_pg();
    var retData = {};
    var client = new pg.Client(conString);


// Now you can start querying your database. Here is a sample.

    client.connect(function(err) {
        if (err) {
            return console.error('could not connect to postgresq',err);
        }
        var query="";
        if(req.body.vehicle_no=="" || req.body.vehicle_no==null || req.body.vehicle_no===undefined){
            query = "SELECT * FROM \"vehicle\";"
        }
        else{
            query = "SELECT * FROM \"vehicle\" where vehicle_no='" + req.body.vehicle_no + "';"
        }
        client.query(query, function(err, result) {
            if (err) {
                return console.err("could not complete query", err);
            }
            client.end();
            console.log(result.rows);
            if(result.rows.length>0){
                res.send(result.rows);
            }
            else{
                res.send(result.rows);
            }

        });
    })
}
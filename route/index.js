
    //<editor-fold desc="Variable Declaration">

    var error_handler = require('./error').errorHandler,
        master_handler = require('./master'),
        express =       require('express'),
        util =          require('../util.js'),
        repo        = require('../repo')
        ;


    var logger =        util.get_logger("server");
    // define routers (new in Express 4.0)
    var ping_route          =   express.Router(),
        master_route        =   express.Router(),
        transaction_route   =   express.Router(),
        help_route          =   express.Router(),
        login_route         =   express.Router()
        ;

    var token = require('../token');
    var login = require('./login');


    //</editor-fold>



    module.exports = exports = function(app, corsOptions) {
        //<editor-fold desc="Fast Phase">
        // protect any route starting with "master" with validation
        app.all("/master/*", authenticate, function(req, res, next) {
            next(); // if the middleware allowed us to get here,
            // just move on to the next route handler
        });

        // implement ping_route actions
        // This is specific way to inject code when this route is called
        // This executes for any ping route.
        // Similarly we can (and should) implement same for every route
        ping_route.use (function(req,res,next){
            logger.info(util.create_routing_log(req.method, req.url, "ping", "PING"));
            // continue doing what we were doing and go to the route
            next();
        });
        ping_route.get('/', function(req, res){
            res.send({'error' : 0, 'msg' : 'audit endpoints ready for data capture'});
        });
        ping_route.get('/check/mongo', function(req, res){
            res.send({'error' : 0, 'msg' : 'Code for checking Mongo connection will be implemented here'});
        });
        ping_route.post('/check/post', function(req, res){
            res.send('Checking Post Method' + req.body);
        });
        app.use('/ping', ping_route);
        // end ping route

        var multipart = require('connect-multiparty');
        var multipartMiddleware = multipart();
        // master route implementation
        master_route.use (function(req,res,next){
            logger.info(util.create_routing_log(req.method, req.url, "master", "MASTER"));
            // continue doing what we were doing and go to the route
            next();
        });
        //</editor-fold>

        //<editor-fold desc="Add Code for new Collection">

        var user_master = master_handler.user_master;
        var api_master = master_handler.api_master;
        //<editor-fold desc="renew token 20141112 srijan">
        master_route.post('/renew/token', function(req, res){
            api_master.renew_token(req,res);
        });
        //</editor-fold>

        //<editor-fold desc="user 20141106 srijan">

        help_route.get('/user', function(req, res){
            user_master.help(req,res);
        });

        // Add Driver

        master_route.post('/driver/add', function(req, res){
            api_master.add_driver(req,res);
        });

        //Update Driver
        master_route.post('/driver/update', function(req, res){
            api_master.update_driver(req,res);
        });

        //get Driver details
        master_route.post('/driver/get', function(req, res){
            api_master.get_driver_details(req,res);
        });

        //add transporter
        master_route.post('/transporter/add', function(req, res){
            api_master.add_transporter(req,res);
        });

        //update transporter
        master_route.post('/transporter/update', function(req, res){
            api_master.update_transporter(req,res);
        });
        //get transporter
        master_route.post('/transporter/get', function(req, res){
            api_master.get_transporter_details(req,res);
        });

        //add organization
        master_route.post('/organization/add', function(req, res){
            api_master.add_organization(req,res);
        });

        //update vehicle
        master_route.post('/vehicle/update', function(req, res){
            api_master.update_organization(req,res);
        });
        //get organization
        master_route.post('/organization/get', function(req, res){
            api_master.get_organization_details(req,res);
        });
        //add vehicle
        master_route.post('/vehicle/add', function(req, res){
            api_master.add_vehicle(req,res);
        });

        //update vehicle
        master_route.post('/vehicle/update', function(req, res){
            api_master.update_vehicle(req,res);
        });
        //get vehicle
        master_route.post('/vehicle/get', function(req, res){
            api_master.get_vehicle_details(req,res);
        });
        //</editor-fold>


        //</editor-fold>


        //<editor-fold desc="Last Phase">

        // implement help_route actions
        help_route.use (function(req,res,next){
            logger.info(util.create_routing_log(req.method, req.url, "help", "HELP"));
            // continue doing what we were doing and go to the route
            next();
        });
        help_route.get('/', function(req, res){
            var r_list = [];
            get_routes(ping_route.stack,r_list, "ping");
            get_routes(master_route.stack,r_list, "master");
            get_routes(help_route.stack,r_list, "help");
            res.send(r_list);
        });
        help_route.get('/show', function(req, res){
            var r_list = [];
            get_routes(ping_route.stack,r_list, "ping");
            get_routes(master_route.stack,r_list, "master");
            get_routes(help_route.stack,r_list, "help");
            //res.send(r_list);
            console.log(r_list);
            res.render('show', {'routes': r_list});
        });
        app.use('/help', help_route);
        // end help route

        //implement login_route action
        login_route.use (function(req,res,next){
            logger.info(util.create_routing_log(req.method, req.url, "login", "LOGIN"));
            // continue doing what we were doing and go to the route
            next();
        });
        //login validation for user
    login_route.post('/validateuser', function(req, res){
        login.validate_user(req,res);
    });


        app.use('/login', login_route);
        app.use('/help', help_route);
        app.use('/master', master_route);

        //end login_route

        // This will be refactored later for adding more features and making it more generic
        var get_routes = function(r, r_list, route_sub_system){
            for (var i=0; i< r.length; i++){
                if (typeof r[i].route != "undefined"){

                    r_list.push (
                        {
                            'path': "/"+ route_sub_system + r[i].route.path,
                            'method':r[i].route.methods
                        }

                    )
                }
            }
            return r_list;
        }

        // and it will validate based on registered data
        function authenticate(req, res, next) {
            var token_data = token.verify(req.headers.token);
            if(token_data){
                next();
            }else{
                res.send('Not authorised');
            }
        }

        // Error handling middleware
        app.use(error_handler);

        //</editor-fold>



    }
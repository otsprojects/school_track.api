/**
 * Created by Developer9 on 3/26/2015.
 */
var config =require('./config/config.json'),
    _ = require('lodash-node')
    ;
log4js = require('log4js');
log4js.configure('./config/log4js.json', {});

exports.get_connection_string_pg = function(){
    return "pg://ots_user:ots@123@54.179.187.189:5432/safetripdev";

}
exports.get_listening_port = function(){
    return "20050";
}
exports.get_logger = function(log){

    return "server";
}

exports.get_logger = function(log){
    var logger = log4js.getLogger(log);
    logger.setLevel(config.logging_level);
    return logger;
}

exports.get_listening_port = function(){
    return config.port;
}

exports.create_routing_log = function (method, url, prefix, route)
{
    var tmp = "Route Sub System: " + route + "\n" + "Method: " + method + "\n" + "URL:" + "/" + prefix  + url;
    return tmp;
}
exports.send_to_response = function(results, res ){
    if (results instanceof Array){
        var arr = [];
        results.forEach(function(r){
            arr.push(r)

        });
    }
    else{
        var arr = results;
    }
    res.contentType('application/json');
    res.send(arr);
}